<?php
/**
 * Copyright © 2016 ShopGo. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace ShopGo\SiteId\Console\Command\SiteId;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;

/**
 * Set site ID command
 */
class Set extends Command
{
    /**
     * XML path general site ID
     */
    const XML_PATH_GENERAL_SITE_ID = 'shopgo_site_id/general/site_id';

    /**
     * Site ID argument
     */
    const SITE_ID = 'site_id';

    /**
     * @var State
     */
    private $state;

    /**
     * @var \ShopGo\SiteId\Model\ConfigFactory
     */
    private $configFactory;

    /**
     * @param State $state
     * @param \ShopGo\SiteId\Model\ConfigFactory $config
     */
    public function __construct(
        State $state,
        \ShopGo\SiteId\Model\ConfigFactory $config
    ) {
        $this->state = $state;
        $this->configFactory = $config;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('shopgo-site-id:set-id')
            ->setDescription('Set site ID command')
            ->setDefinition([
                new InputArgument(
                    self::SITE_ID,
                    InputArgument::REQUIRED,
                    'Site ID'
                )
            ]);

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode('adminhtml');

        $siteId = $input->getArgument(self::SITE_ID);

        $config = $this->configFactory->create();
        $result = $config->setConfigData(self::XML_PATH_GENERAL_SITE_ID, $siteId);

        $result = $result
            ? "Site ID has been set!"
            : "Could not set site ID!";

        $output->writeln('<info>' . $result . '</info>');
    }
}
