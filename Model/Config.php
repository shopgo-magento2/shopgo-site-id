<?php
/**
 * Copyright © 2016 ShopGo. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace ShopGo\SiteId\Model;

/**
 * Config model
 */
class Config extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var \Magento\Config\Model\Config\Factory
     */
    protected $configFactory;

    /**
     * @param \Magento\Config\Model\Config\Factory $configFactory
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     */
    public function __construct(
        \Magento\Config\Model\Config\Factory $configFactory,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
    ) {
        $this->configFactory = $configFactory;
        $this->cacheTypeList = $cacheTypeList;
    }

    /**
     * Get config model
     *
     * @param array $configData
     * @return \Magento\Config\Model\Config
     */
    protected function getConfigModel($configData = [])
    {
        /** @var \Magento\Config\Model\Config $configModel */
        $configModel = $this->configFactory->create(['data' => $configData]);
        return $configModel;
    }

    /**
     * Get config data value
     *
     * @param string $path
     * @return string
     */
    public function getConfigData($path)
    {
        return $this->getConfigModel()->getConfigDataValue($path);
    }

    /**
     * Set config data
     *
     * @param string $path
     * @param string $value
     * @return bool
     */
    public function setConfigData($path, $value)
    {
        $result = false;

        try {
            $path = explode('/', $path);

            $group = [
                $path[1] => [
                    'fields' => [
                        $path[2] => [
                            'value' => $value
                        ]
                    ]
                ]
            ];

            $configData = [
                'section' => $path[0],
                'website' => null,
                'store'   => null,
                'groups'  => $group
            ];

            $this->getConfigModel($configData)->save();
            $this->cacheTypeList->cleanType('config');

            $result = true;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
        } catch (\Exception $e) {}

        return $result;
    }
}
